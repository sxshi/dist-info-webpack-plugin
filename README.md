# 插件简介

一款用于代码打包时，将打包信息注入html内的插件，通过此插件，可以很方便的查看代码的打包信息。
支持git以及常见的流水线构建工具，如coding等。

# 插件演示

浏览器控制台输入"**buildInfo**"时，控制台会自动打印出构建信息。
![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/cc6c8d135f1043499add9b370c4f828c~tplv-k3u1fbpfcp-watermark.image?)

# 使用
项目中引入插件

```js
npm  i dist-info-webpack-plugin -s
```

以vue项目为例，在vue.config.js中

```js
const distInfo = require("dist-info-webpack-plugin");
module.exports = {
    configureWebpack: {
        new distInfo()
    }
}
```
开发模式或生产模式下，控制台输入"**buildInfo**"即可查看构建信息。

> 注：如果生产环境作为了qiankun子应用，输入**buildInfo**会报错，因为qiankun将子应用的window对象代理到了proxy上，因此，此时输入**proxy.buildInfo**。

# 可选配置


```js
new packageInfo({
    // 指令名称。
    name: "buildInfo",
    // 是否立即将构建信息输出在控制台
    immediate: fasle,
    // 是否保留预设值
    preset: true,
    // 自定义要展示的数据
    consoleList: [
      ....
    ]
})
```


| 属性名 | 类型 | 释义 | 默认值 |
| --- | --- | --- | --- | 
| name | string | 控制台输入的指令名称 | "buildInfo" |
| immediate | boolean |  是否立即将构建信息输出在控制台 | fasle |
| preset | boolean | 是否使用预设的打印信息 | true |
| consoleList | Array | 用户自定义的打印信息 | [] |

## name 
默认情况下，在控制台输入**buildInfo**即可打印出构建信息。这个值可以根据您的情况更改，如下，当您设置为**info**时，需要在控制台输入**info**才能打印出构建信息。
```js
new packageInfo({
    // 指令名称。
    name: "info",
})
```

![GIF 2023-1-15 9-36-54.gif](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7bc615bfaf3549b8933141b975621cc3~tplv-k3u1fbpfcp-watermark.image?)

## immediate

默认情况下，需要在控制台输入指令才能打印构建信息，如果您希望构建信息直接输出在控制台，您可以将其设置为true。如图，刷新页面时，构建信息被直接打印在控制台。
```js
new packageInfo({
    // 指令名称。
    name: "info",
    immediate: true,
})
```

![GIF 2023-1-15 9-41-24.gif](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/9869d9e437c8440baffdef5c5888e7fd~tplv-k3u1fbpfcp-watermark.image?)


> 为了信息安全，建议在生产环境不要将此值设置为ture。或者可以这样设置：**immediate: process.env.NODE_ENV === "development"**


## consoleList
通过此项，您可以自定义要打印的信息。

```js
plugins: [
  new packageInfo({
    name: "info",
    // immediate: true,
    // 自定义要展示的数据
    consoleList: [
      {
        description: "这是一个自定义消息111！！",
        value: `当前环境为${process.env.NODE_ENV}`,
        mode: "development",
      },
      {
        description: "这是一个自定义消息222！！",
        value: "这是个测试消息---------------",
        mode: "development",
      },
    ],
  }),
],
```

![GIF 2023-1-15 9-50-22.gif](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/3ee3fb1588644caab5b8d4b3eba468b9~tplv-k3u1fbpfcp-watermark.image?)

consoleList的每一个对象有三个属性，description，value，mode。
mode用于筛选，如果设置为mode: "development",自定义信息只会在开发模式下打印。如果设置为mode: "production"，则只会在代码打包后显示。

**mode可以不设置，默认为"production"。**

### 如何打印codding的构建信息

您可能关注使用codding流水线时，如何打印想要的值。

非常简单，您可以根据官方文档查看有哪些可用环境变量[# coding环境变量说明](https://coding.net/help/docs/ci/configuration/env.html)

比如，官方提供了 **GIT_COMMIT_SHORT**环境变量（修订版本号的前 7 位），打印时，您只需要加上
**process.env.GIT_COMMIT_SHORT**即可。
```js
plugins: [
  new packageInfo({
    name: "info",
    // immediate: true,
    // 自定义要展示的数据
    consoleList: [
      {
        description: "代码版本号",
        value: process.env.GIT_COMMIT_SHORT,
        mode: "production",
      },
    ],
  }),
],
```

> **其他流水线工具配置是一样的**

# preset
默认情况下，插件已经内置了打印的构建信息，如果您不需要，只想使用自己定义的信息，将其设置为false即可。

```js
plugins: [
  new packageInfo({
    name: "info",
    // immediate: true,
    // 自定义要展示的数据
    consoleList: [
      {
        description: "这是一个自定义消息111！！",
        value: `当前环境为${process.env.NODE_ENV}`,
        mode: "development",
      },
      {
        description: "这是一个自定义消息222！！",
        value: "这是个测试消息---------------",
        mode: "development",
      },
    ],
    preset:false
  }),
],
```


![GIF 2023-1-15 10-04-36.gif](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/36f89c0a8b3243aeaa5a78a64c0e1a12~tplv-k3u1fbpfcp-watermark.image?)